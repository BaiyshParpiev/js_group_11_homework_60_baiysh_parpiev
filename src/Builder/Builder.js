import React, {useState} from 'react';
import {useEffect} from "react";
import Input from "../Components/Input/Input";
import Messages from "../Components/Messages/Messages";
import axios from 'axios'

const url = 'http://146.185.154.90:8000/messages';
let data = '';
const Builder = () => {
    const [value, setValue] = useState('');
    const [messages, setMessages] = useState([]);
    const [error, setError] = useState(null);

    const onHandlerChange = e => {
        setValue(e.target.value)
    }

    const onSendMessage = async () => {
        try {
            const data = new URLSearchParams();
            data.set('author', 'It-is-me');
            data.set('message', value);
            const response = await axios.post(url, data)
            setError(null);
            setValue('');
        } catch (e) {
            console.log(e.response);
            setError('Something is wrong' + e.response.status)
        }
    }

    useEffect(() => {
        const getData = async () => {
            try {
                const response = await axios.get(url);
                const responseMessages = response.data;
                setMessages(prev => {
                    return responseMessages
                });
                data = responseMessages[responseMessages.length - 1].datetime

                setInterval(async () => {
                    const newMessages = await axios.get(url + '?datetime=' + data);
                    const newData = newMessages.data
                    if (newData.length > 0) {
                        setMessages(prev => ([
                            ...prev,
                            ...newData,
                        ]))
                        data = newData[newData.length - 1].datetime;
                    } else {
                        return null;
                    }
                    window.scrollTo(0, document.body.scrollHeight);
                }, 4000)
                setError(null)
            } catch (e) {
                console.log(e.response);
                setError('Something is wrong' + e.response.status)
            }
        }
        getData().catch(e => console.log(e))
    }, []);


    return (
        <div className="container">
            {error && (<div style={{backgroundColor: 'red', color: 'white', padding: '15px'}}>{error}</div>)}
            <div className="listOf-messages">
                {messages.map(m => (
                    <Messages id={m.id} label={m.message} class={m.author}/>
                ))}
            </div>
            <div className="send-message">
                <Input value={value} changeHandler={e => onHandlerChange(e)} click={onSendMessage}/>
            </div>
        </div>
    );
};

export default Builder;